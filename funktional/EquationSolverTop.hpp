#ifndef INCLUDED_EQUATION_SOLVER_TOP
#define INCLUDED_EQUATION_SOLVER_TOP

#include "EquationSolver.hpp"
#include "EquationSolverTest.hpp"

#define SC_INCLUDE_FX
#include <systemc.h>
#include <iostream>

template<class T>
class EquationSolverTop : public sc_module {
public:
  EquationSolverTop(sc_module_name name, const char* datafile)
    : sc_module(name),
      solver("solver"),
      test("test", datafile)
  {
    solver.x0_in(x0);
    solver.u0_in(u0);
    solver.y0_in(y0);
    solver.dx_in(dx);
    solver.a_in(a);
    solver.y_out(y);
    solver.start(start);
    solver.ready(ready);
    
    test.x0_out(x0);
    test.u0_out(u0);
    test.y0_out(y0);
    test.dx_out(dx);
    test.a_out(a);
    test.y_in(y);
    test.start(start);
    test.ready(ready);
  }

private:
  EquationSolver<T> solver;
  EquationSolverTest<T> test;

  sc_signal<T> x0;
  sc_signal<T> u0;
  sc_signal<T> y0;
  sc_signal<T> dx;
  sc_signal<T> a;
  sc_signal<T> y;

  sc_signal<bool> ready;
  sc_signal<bool> start;
};

#endif // INCLUDED_EQUATION_SOLVER_TOP
