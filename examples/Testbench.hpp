class TestBench : public sc_module {
private:
	Generator generator;
	DUV       duv;
	Monitor   monitor;

public:
	sc_fifo<double> f1;
	sc_fifo<double> f2;

	TestBench(sc_module_name name, int m)
	: sc_module(name),
	  generator("generator", m),
	  
	  monitor("monitor"),duv("duv"),
	  f1(2),
	  f2(2) {
		generator.out(f1);
		duv.in(f1);
		duv.out(f2);
		monitor.in(f2);
	}
};
