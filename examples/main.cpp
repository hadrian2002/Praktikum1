#include "systemc.h"
#include "DUV.hpp"
#include "Generator.hpp"
#include "Monitor.hpp"
#include "Testbench.hpp"


int sc_main (int argc, char **argv) {
	TestBench testbench("testbench", 5);
	sc_start();
	return 0;
}
